### 开心品生活-客户端

本项目是基于` uniapp `模板 [https://ext.dcloud.net.cn/plugin?id=1988](https://ext.dcloud.net.cn/plugin?id=1988) 开源的后端程序

原模板基于` MIT `协议，因此本仓库也遵守 `MIT` 协议。

api配置：config/api.js

#### 一、项目架构

本仓库地址：[https://gitee.com/shayvmo/happy-life](https://gitee.com/shayvmo/happy-life)

#### 二、Git 提交说明

[!] 修改BUG

[+] 增加功能

[*] 普通修改

[-] 减少功能

#### 三、参与开源

1、fork 仓库

2、推送代码到 develop 分支，提交Pull Request


#### 四、项目交流

QQ：1006897172

或「 开心品生活 」原作者QQ交流群：1054938991

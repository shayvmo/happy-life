// let newApiRoot = 'http://pear.test/api/';
let newApiRoot = 'https://capital.shayvmo.cn/api/';
let api = {
	common: {
		aboutUs: newApiRoot + 'common/aboutUs',
	},
	article: {
		index: newApiRoot + 'article/index',
		category: newApiRoot + 'article/category',
		detail: newApiRoot + 'article/detail',
		comment: newApiRoot + 'article/comment',
		commentDetail: newApiRoot + "article/commentDetail",
		commentReply: newApiRoot + "article/commentReply",
		addComment: newApiRoot + 'article/addComment',
		addReply: newApiRoot + 'article/addReply',
		like: newApiRoot + "article/like",
		commentLike: newApiRoot + "article/commentLike",
		commentReplyLike: newApiRoot + "article/commentReplyLike",
		favorite: newApiRoot + "article/favorite",
		favoriteList: newApiRoot + "article/favoriteList",
	},
	user: {
		index: newApiRoot + "user/index",
		feedback: newApiRoot + "user/feedback",
		updateInfo: newApiRoot + "user/updateInfo",
	},
	wechat: {
		miniAppLogin: newApiRoot + 'wechat/miniAppLogin',
	},
};
export default api;
